try {
    function itemWithVitaminC(items) {
        if (Array.isArray(items)) {

            let itemList = items.filter((item) => item.contains.includes("Vitamin C") && item.contains.length == 9)
            .map((item) => item.name);
            return itemList
        }
        else {
            console.log("Pass Valid Arguments as parameter");
            return null;
        }
    }

} catch (error) {
    console.log("Somethig Went wrong !");

}
module.exports = itemWithVitaminC;