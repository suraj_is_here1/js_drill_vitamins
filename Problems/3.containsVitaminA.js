try {
    function itemWithVitaminA(items) {
        if (Array.isArray(items)) {

            let itemList = items.filter((item) => item.contains.includes("Vitamin A"))
            .map((item) => item.name);
            return itemList
        }
        else {
            console.log("Pass Valid Arguments as parameter");
            return null;
        }
    }

} catch (error) {
    console.log("Somethig Went wrong !");

}
module.exports = itemWithVitaminA;