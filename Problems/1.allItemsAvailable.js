try {
    function isAvailable(items) {
        if (Array.isArray(items)) {

            let itemList = items.filter((item) => item.available == true)
            .map((item) => item.name);

            return itemList;
        }
        else {
            console.log("Pass Valid Arguments as parameter");
            return null;
        }
    }

} catch (error) {
    console.log("Somethig Went wrong !");

}
module.exports = isAvailable;