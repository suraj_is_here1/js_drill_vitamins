try {
    function itemWithMaximumVitamin(items) {
        if (Array.isArray(items)) {

            let itemList = items.filter((item) => item.contains.includes("Vitamin"))
            .sort((item1,item2) => item2.contains.length - item1.contains.length)
            .map((item) => item.name);
            
            return itemList
        }
        else {
            console.log("Pass Valid Arguments as parameter");
            return null;
        }
    }

} catch (error) {
    console.log("Somethig Went wrong !");

}
module.exports = itemWithMaximumVitamin;