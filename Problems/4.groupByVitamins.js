try {
    function groupByVitamins(items) {
        if (Array.isArray(items)) {

            let vitaminswithFruits = items.reduce((acc, currItem) => {
                let list = (currItem.contains).split(", ");

                list.reduce((acc2, currVitamin) => {
                    if (!acc2[currVitamin]) {
                        acc2[currVitamin] = [];
                    }
                    acc2[currVitamin].push(currItem.name);
                    return acc2;
                }, acc);
                return acc;
            }, {});
            return vitaminswithFruits;
        }
        else {
            console.log("Pass Valid Arguments as parameter");
            return null;
        }
    }

} catch (error) {
    console.log("Somethig Went wrong !");
}
module.exports = groupByVitamins;
